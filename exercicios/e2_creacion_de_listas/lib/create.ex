defmodule Create do
    def create(n) when n > 0, do: for i <- 1..n, do: i
    def create(n), do: []
    def reverse_create(n) when n > 0, do: for i <- n..1, do: i
    def reverse_create(n), do: []
end
