defmodule Measure do
    import Enum
    def generate({_,:flatten},n), do: for i <- 1..10, do: for j <- 1..n, do: :rand.uniform(1000)
    def generate({_,f},n), do: for i <- 1..n, do: :rand.uniform(1000)

    def results(task) do
        case Task.yield(task) do
            {:ok,result} -> result
            nil -> results(task)
        end
    end

    def results2(task) do
        case Task.yield(task,10000) do
            {:ok,result} -> result
            nil -> "interrumpido"
        end
    end

    def run(funcs,n) do
        tasks = for i <- funcs, do: Task.async(fn -> generate(i,n) end)
        time1 = :erlang.timestamp
        results = for i <- tasks, do: results(i)
        timediff = :erlang.timestamp |> :timer.now_diff(time1)
        IO.puts "Creación de datos: #{timediff}    ms"
        zipped = Enum.zip(funcs,results)
        tasks2 = for {{m,f},r} <- zipped, do: Task.async(m,f,[r])
        results = for i <- tasks2 do
            time1 = :erlang.timestamp
            res = results2(i)
            timediff = :erlang.timestamp |> :timer.now_diff(time1)
            case res do
                "interrumpido" -> {"interrumpido",nil}
                a -> {timediff,res}
            end

        end

        toprint = :lists.zip(funcs,results)

        for {{m,f},{t,r}} <- toprint, do: IO.puts "#{m}:#{f}  : #{t}  ms"
    end
end
