defmodule Effects do
    def print(n), do: print(n,1)
    
    defp print(n,acc) when acc <= n do
        IO.puts acc
        print(n,acc+1)
    end

    defp print(n,acc) do
        IO.write ""
    end

    def even_print(n), do: print_even(n,2)

    defp print_even(n,acc) when acc <=n do
        IO.puts acc
        print_even(n,acc+2)
    end

    defp print_even(n,acc) do
        IO.write ""
    end
end
