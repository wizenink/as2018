defmodule Echo do
    def start() do
        pid = spawn fn -> server() end
        Process.register(pid,:echo)
        :ok
    end
    
    def stop() do
        send :echo, {:stop}
        Process.unregister(:echo)
        :ok
    end

    def print(term) do
        send :echo, {:print,term}
        :ok
    end

    defp server() do
        receive do
            {:stop} -> :ok
            {:print,term} -> 
                IO.puts term
                server()
        end
    end
end
