defmodule Sorting do
    def quicksort([]), do: []
    def quicksort([h|t]) do
        l = for i <- t,i<=h, do: i
        r = for i <- t,i>h, do: i
        quicksort(l) ++ [h|quicksort(r)]
    end

  def mergesort(list) when length(list) <= 1, do: list
  def mergesort(list) do
    {left, right} = Enum.split(list, div(length(list), 2))
    :lists.merge( mergesort(left), mergesort(right))
  end
end
