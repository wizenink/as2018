defmodule Manipulating do
    def filter(l,n), do: for i <- l, i <= n, do: i

    def reverse(l), do: reverse(l,[])
    defp reverse([h|t],acc), do: reverse(t,[h|acc])
    defp reverse([],acc), do: acc


    #def flatten(l), do: concatenate(l,-2)
    #def concatenate(l), do: concatenate(l,-2)
    #def concatenate(list, depth), do: concatenate(list, depth + 1, []) |> Enum.reverse
    #def concatenate(list, 0, acc), do: [list | acc]
    #def concatenate([h | t], depth, acc) when h == [], do: concatenate(t, depth, acc)
    #def concatenate([h | t], depth, acc) when is_list(h), do: concatenate(t, depth, concatenate(h, depth - 1, acc))
    #def concatenate([h | t], depth, acc), do: concatenate(t, depth, [h | acc])
    #def concatenate([], _, acc), do: acc

    def concatenate(l), do: concatenate(l,[]) |> reverse
    defp concatenate([[h2|t2]|t],acc), do: concatenate([t2|t],[h2|acc])
    defp concatenate([[]|t],acc), do: concatenate(t,acc)
    defp concatenate([],acc), do: acc

    def flatten(l), do: flatten(l,[]) |> reverse
    defp flatten([[h2|t2]|t],acc), do: flatten([h2|[t2|t]],acc)
    defp flatten([[]|t],acc), do: flatten(t,acc)
    defp flatten([h|t],acc), do: flatten(t,[h|acc])
    defp flatten([],acc), do: acc
end
