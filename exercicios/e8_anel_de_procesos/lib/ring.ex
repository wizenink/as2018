defmodule Ring do

    def start(n,m,msg) do
        plist = for n <- 1..n do spawn fn -> ringElement(m) end end
        #IO.puts "LIST IS: #{inspect plist}"
        send Enum.at(plist,0), {plist,msg,1}
        :ok
    end

    defp ringElement(limit) do
        receive do
            {[next | t ],msg,acc} when acc <= limit ->
                #IO.puts "Received #{msg} count #{acc}"
                IO.puts msg
                send next, {t ++ [next],msg,acc+1}
                ringElement(limit)
            {[next|t],_,_} ->
                send next, {t,:ok}
            {[next|t],:ok} ->
                send next, {t,:ok}
        end
    end
end
