defmodule Db do
    def new(), do: []
    def destroy(ref), do: :ok
    def write(ref,key,element), do: [{key,element}|ref]
    def delete(ref,key), do: for {k,element} <- ref, k != key, do: {k,element}
    def read([{k,e}|t],key) when k == key, do: {:ok,e}
    def read([h|t],key), do: read(t,key)
    def read([],key), do: {:error,:not_found}
    def match(ref,element), do: for {key,el} <- ref, el == element, do: key
end
